/*! @file Welcome.h

  @brief   global header file for project
  @author  Hendrik Schutter
  @version V1.0
  @date    14.11.2020

This file includes other header files used in this project.
*/

#ifndef __ES2_V5_APP_H
#define __ES2_V5_APP_H

#include <stdio.h>
#include "LPC17xx.H"
#include "GLCD_Config.h"
#include "Board_GLCD.h"
#include "Board_LED.h"
#include "Board_Buttons.h"
#include "stdint.h"

//-------------------------------------------------------------------
// defines
//-------------------------------------------------------------------

#define TEXTLENGTH_SHORT   30       // Length of short text

//-------------------------------------------------------------------
// function declarations

//-------------------------------------------------------------------

//library globals - always external keyword

extern GLCD_FONT GLCD_Font_16x24;
extern GLCD_FONT GLCD_Font_6x8;	// GLCD Font from GLCD_Fonts.c

extern uint32_t u32Volume;

// project globals

#endif /* __ES2_V5_APP_H */
