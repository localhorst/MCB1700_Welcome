# MCB1700_Welcome

## Overview
Displays URL and Qr code on LCD Display from MCB1700.
Plays melody from "Never Gonna Give You Up".

## Target 
Keil MCB1700

## QR Code
25x25 QR Code Version 2

<img src="https://git.mosad.xyz/localhorst/MCB1700_Welcome/raw/commit/7e2b6c8c80069134e360b6e00996d2af932565d1/qr.png" alt="code_example_output" width="150"/>

## Documentation
In the  [Doxygen](https://git.mosad.xyz/localhorst/MCB1700_Welcome/src/branch/master/Doxygen/html) directory you can find HTML Doxygen Documentation.

## Demo
<img src="https://git.mosad.xyz/localhorst/MCB1700_Welcome/raw/commit/7e2b6c8c80069134e360b6e00996d2af932565d1/mcb1700.png" alt="code_example_output" width="500"/>

## LICENSE
[Beerware](https://git.mosad.xyz/localhorst/MCB1700_Welcome/src/branch/master/LICENSE) LICENSE