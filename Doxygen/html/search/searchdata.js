var indexSectionsWithContent =
{
  0: "_abcdfghijmopqrstuvwx",
  1: "a",
  2: "agmrsw",
  3: "bmstv",
  4: "abcdfghjmqrstu",
  5: "_cfioprstuwx",
  6: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

