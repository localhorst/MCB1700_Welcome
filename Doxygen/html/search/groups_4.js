var searchData=
[
  ['taskdisable_5finterrupts_3150',['taskDISABLE_INTERRUPTS',['../group__task_d_i_s_a_b_l_e___i_n_t_e_r_r_u_p_t_s.html',1,'']]],
  ['taskenable_5finterrupts_3151',['taskENABLE_INTERRUPTS',['../group__task_e_n_a_b_l_e___i_n_t_e_r_r_u_p_t_s.html',1,'']]],
  ['taskenter_5fcritical_3152',['taskENTER_CRITICAL',['../group__task_e_n_t_e_r___c_r_i_t_i_c_a_l.html',1,'']]],
  ['taskexit_5fcritical_3153',['taskEXIT_CRITICAL',['../group__task_e_x_i_t___c_r_i_t_i_c_a_l.html',1,'']]],
  ['taskhandle_5ft_3154',['TaskHandle_t',['../group___task_handle__t.html',1,'']]],
  ['taskyield_3155',['taskYIELD',['../group__task_y_i_e_l_d.html',1,'']]]
];
