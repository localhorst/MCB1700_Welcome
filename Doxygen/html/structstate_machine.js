var structstate_machine =
[
    [ "bStateChanged", "group___s_m.html#ga7d48771d20211e9a6405911d8e41db8c", null ],
    [ "psStateTable", "group___s_m.html#gae7907afecdc76d8302dad95d713aef07", null ],
    [ "s32ActualState", "group___s_m.html#gae46d0848e5bf7496e27f4b6c355bc86b", null ],
    [ "s32NextState", "group___s_m.html#ga197f9575f7a3a8638622e7dc5fc7be58", null ],
    [ "u32EventCount", "group___s_m.html#gac0ab152443940c2fcd919e59b9ee87a7", null ],
    [ "u32StateCount", "group___s_m.html#gab465e2b8a2f0ef43b549a66a0e91245a", null ],
    [ "u32StateMachineId", "group___s_m.html#ga8c38634e13a564a3a2cd28c7ebf9dde7", null ]
];