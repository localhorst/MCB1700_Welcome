var _hei_o_s___settings_8h =
[
    [ "CHECK_THREAD_STACK", "_hei_o_s___settings_8h.html#addb9c74ead6f98dd95aaebaf8823ca1a", null ],
    [ "DISABLE_INTERRUPT_NUMBER", "_hei_o_s___settings_8h.html#a235e1068057e01693d8744ed64023df0", null ],
    [ "FIXED_PRIORITY", "_hei_o_s___settings_8h.html#ac733336d41e8f42047e58d8df2f50162", null ],
    [ "IDLE_THREADSTACKSIZE", "_hei_o_s___settings_8h.html#ae8c7ca0c06d95dc7b400ac278de7adb1", null ],
    [ "LOTTERY", "_hei_o_s___settings_8h.html#a132656d306c3b5768f79fbd0827d8a78", null ],
    [ "ROUNDROBIN", "_hei_o_s___settings_8h.html#af1d48ecc012cc80477ac9fe804e3775c", null ],
    [ "THREAD_STACK_MARK", "_hei_o_s___settings_8h.html#afbbf897db60f712f08ab7936dbcc19a0", null ],
    [ "THREADNUMBER", "_hei_o_s___settings_8h.html#ad89c89f9a8ae893ca26c208ad005751a", null ],
    [ "TICKRATE", "_hei_o_s___settings_8h.html#a7f21fe9fdb3605017d51d802b41910ac", null ]
];