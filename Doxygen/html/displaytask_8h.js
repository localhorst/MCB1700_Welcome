var displaytask_8h =
[
    [ "GLCD_Param_uint32_t", "struct_g_l_c_d___param__uint32__t.html", "struct_g_l_c_d___param__uint32__t" ],
    [ "GLCD_Param_GLCD_FONT", "struct_g_l_c_d___param___g_l_c_d___f_o_n_t.html", "struct_g_l_c_d___param___g_l_c_d___f_o_n_t" ],
    [ "GLCD_Param_bool", "struct_g_l_c_d___param__bool.html", "struct_g_l_c_d___param__bool" ],
    [ "GLCD_Param_2_uint32_t", "struct_g_l_c_d___param__2__uint32__t.html", "struct_g_l_c_d___param__2__uint32__t" ],
    [ "GLCD_Param_3_uint32_t", "struct_g_l_c_d___param__3__uint32__t.html", "struct_g_l_c_d___param__3__uint32__t" ],
    [ "GLCD_Param_2_uint32_t_int32_t", "struct_g_l_c_d___param__2__uint32__t__int32__t.html", "struct_g_l_c_d___param__2__uint32__t__int32__t" ],
    [ "GLCD_Param_2_uint32_t_char_p", "struct_g_l_c_d___param__2__uint32__t__char__p.html", "struct_g_l_c_d___param__2__uint32__t__char__p" ],
    [ "GLCD_Param_4_uint32_t", "struct_g_l_c_d___param__4__uint32__t.html", "struct_g_l_c_d___param__4__uint32__t" ],
    [ "GLCD_Param_5_uint32_t", "struct_g_l_c_d___param__5__uint32__t.html", "struct_g_l_c_d___param__5__uint32__t" ],
    [ "GLCD_Param_4_uint32_t_uint8_t_p", "struct_g_l_c_d___param__4__uint32__t__uint8__t__p.html", "struct_g_l_c_d___param__4__uint32__t__uint8__t__p" ],
    [ "GLCD_Command", "struct_g_l_c_d___command.html", "struct_g_l_c_d___command" ],
    [ "DisplayCommand", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3be", [
      [ "cmd_GLCD_Initialize", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea1aa81ac4469eed423bee4c3beb31d1eb", null ],
      [ "cmd_GLCD_Uninitialize", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea95b0af254dbace57347fdeff15aee5d2", null ],
      [ "cmd_GLCD_SetForegroundColor", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3beabee6e5dc3eca49964c1ea65a043a43be", null ],
      [ "cmd_GLCD_SetBackgroundColor", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3beabd791d3e67ed811a1c9899fdc84dc71e", null ],
      [ "cmd_GLCD_ClearScreen", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea6ff9c58f8b7db8f6f722b6630e233d64", null ],
      [ "cmd_GLCD_SetFont", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea56dacee8e57a8e682730ef8ee58c0ef1", null ],
      [ "cmd_GLCD_DrawPixel", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea907a849ae3aca6a100069ebac04d10f2", null ],
      [ "cmd_GLCD_DrawHLine", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea7bda7792eab4ba0fd3065ef604482c39", null ],
      [ "cmd_GLCD_DrawVLine", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea96d997ec484077ebcce0277292c69520", null ],
      [ "cmd_GLCD_DrawRectangle", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea66032db162d83696284af45e0b055d34", null ],
      [ "cmd_GLCD_DrawChar", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea00be27b5ee37092fde6dc5794b06c9de", null ],
      [ "cmd_GLCD_DrawString", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3beac5f62c2a85c6650051160d3696517c46", null ],
      [ "cmd_GLCD_DrawBargraph", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3beafdb042722f657bd39fb1da044488a4b0", null ],
      [ "cmd_GLCD_DrawBitmap", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea547ad4454e6daa5f02c2df3605390d91", null ],
      [ "cmd_GLCD_VScroll", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea1201c7c0a2d30e451f9ebf0b7f583edb", null ],
      [ "cmd_GLCD_FrameBufferAccess", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea7588d80f4a4765dcdb066e7e0a4eb0d3", null ],
      [ "cmd_GLCD_FrameBufferAddress", "displaytask_8h.html#ab0433629f57aaf649227bc0b74f0e3bea45e72f9f7282139ae288ed4535ffeb6d", null ]
    ] ],
    [ "vDisplayTask", "displaytask_8h.html#a3a1d2a0575667f1aadaf57140d32c010", null ]
];