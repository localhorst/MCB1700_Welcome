var _fsm_v_c_r_8h =
[
    [ "eEventsVCR_t", "_fsm_v_c_r_8h.html#a2f4513892887e3fb5c21f8daa5f3bcc9", null ],
    [ "eStatesVCR_t", "_fsm_v_c_r_8h.html#ae95ff39fa9c38c0f41eafe329192e584", null ],
    [ "fsmVCRevents", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3", [
      [ "evPlaybutton", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3acc1ff6e650f3b4d11874319eb5dc5c0e", null ],
      [ "evStopbutton", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a790523a2662a9545021a742037500f69", null ],
      [ "evFastforwardbutton", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a528e4b792688bb9689100c138ba880bb", null ],
      [ "evRecordbutton", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a047e32bcf7ea9b4d7f58f7dc43b66a59", null ],
      [ "evRewindbutton", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a24987c0c28c43668b03e3bab42543e9b", null ],
      [ "evBegintape", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3aaa562225032e76d3e4a7f498d5d3dab7", null ],
      [ "evEndtape", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3ad13e241463be31ddbb1e5bc9f9386721", null ],
      [ "evTapeout", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a314c5f2a242a0427991e1cfdd775c176", null ],
      [ "EV_LAST_VCR", "_fsm_v_c_r_8h.html#a8729d0ca8ba69e9d41555cf301a3dca3a93d72d4e5500c25818922c7589506227", null ]
    ] ],
    [ "fsmVCRstates", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3", [
      [ "sStandby", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3a238ea94c478b6f0a5cd4ceac79dd06e6", null ],
      [ "sRecord", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3a01cb2583ee3b2bead328eb6089d6cc2b", null ],
      [ "sPlay", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3a5b9eafe39cf54570c81dba3526efcd89", null ],
      [ "sRewind", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3a2921dbe78f201c8b013d0566d0ea859d", null ],
      [ "sFastForward", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3ad8f56de0d99a58f4694d16bd0cdb9323", null ],
      [ "STATE_LAST_VCR", "_fsm_v_c_r_8h.html#a20e2b1bd99296af90f99a111aae31bc3a1db91b6653034e7db39a5047ede1e3fe", null ]
    ] ],
    [ "bFsmVCRDispatchEvent", "_fsm_v_c_r_8h.html#a7efd0fb7272aa90b21cb7bba64ce1241", null ],
    [ "bFsmVCRInitialize", "_fsm_v_c_r_8h.html#a222f4d8dc38c7189da1ec07eaec8308e", null ],
    [ "bFsmVCRIsStateChanged", "_fsm_v_c_r_8h.html#a1af893c066add85104fc8093504df0d5", null ],
    [ "bGuard_tapein_andnot_atbegin", "_fsm_v_c_r_8h.html#a698d66c10ea05d79d2d41b2e3f7148c3", null ],
    [ "bGuard_tapein_andnot_atend", "_fsm_v_c_r_8h.html#a26e841d7379310a74190a21d88212813", null ],
    [ "pcFsmVCRGetStateName", "_fsm_v_c_r_8h.html#aa966be54c30b5d074616e0c5466b9d36", null ],
    [ "s32FsmVCRGetState", "_fsm_v_c_r_8h.html#af424a50960bfab12903bb41be9584e74", null ],
    [ "vEntry_sFastForward", "_fsm_v_c_r_8h.html#a65ae13eda4bb8013456f604d85395bda", null ],
    [ "vEntry_sPlay", "_fsm_v_c_r_8h.html#a17f26377e10db9a38550ef506e9106d3", null ],
    [ "vEntry_sRecord", "_fsm_v_c_r_8h.html#a4b9e2f1efe6a4aeec436a520ec448a11", null ],
    [ "vEntry_sRewind", "_fsm_v_c_r_8h.html#acf9bc6ae5c1d7fb96e6db9b721aa7ea7", null ],
    [ "vEntry_sStandby", "_fsm_v_c_r_8h.html#a376b0ceba531fad362bdc20398eacb5b", null ],
    [ "vExit_sFastForward", "_fsm_v_c_r_8h.html#a8831de5abebe3920b328d643b58d924b", null ],
    [ "vExit_sPlay", "_fsm_v_c_r_8h.html#a9124ca3635cfd75274e23c9f0fe64cb2", null ],
    [ "vExit_sRecord", "_fsm_v_c_r_8h.html#af78c27448b757fc82b68e327e7022ea3", null ],
    [ "vExit_sRewind", "_fsm_v_c_r_8h.html#ac1d2d423300ca1a64457aaa07dd1674f", null ]
];