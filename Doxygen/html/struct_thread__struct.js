var struct_thread__struct =
[
    [ "delay", "struct_thread__struct.html#a458421a43d4f6dc515faf427bf579d00", null ],
    [ "executiontime", "struct_thread__struct.html#a906030244e56503e938653af4e6f1767", null ],
    [ "identity", "struct_thread__struct.html#a79862aa6064cbbefdb152678f6f17d6f", null ],
    [ "name", "struct_thread__struct.html#a9997d8ee2df51d18efed254a5aa016fd", null ],
    [ "next", "struct_thread__struct.html#a8f9c33c6996e4f010b01e2af17502888", null ],
    [ "pfunc", "struct_thread__struct.html#a31bdae9c75c66a1824ad271b469eacc4", null ],
    [ "priority", "struct_thread__struct.html#a0ad043071ccc7a261d79a759dc9c6f0c", null ],
    [ "PSP", "struct_thread__struct.html#a2cb2d01ba77893b8843ad57a2155ba2e", null ],
    [ "ssize", "struct_thread__struct.html#aa3feed022ffd137bf57085f9442105e8", null ],
    [ "stack", "struct_thread__struct.html#a1abf78464029077a8080178b9e04745c", null ],
    [ "tickcount", "struct_thread__struct.html#a3d67397d99f6243c885906f630e2e2e3", null ]
];