var struct_g_l_c_d___command =
[
    [ "eDisplayCommand", "struct_g_l_c_d___command.html#ab37302e234bdaa92f4cdc7dcbdece43b", null ],
    [ "sparam_2_uint32_t", "struct_g_l_c_d___command.html#a4ac97d4b814e2ca5c65b495ad9b5208f", null ],
    [ "sparam_2_uint32_t_const_char_p", "struct_g_l_c_d___command.html#a3bd0a009b327b1ec7c6e6fe30a8d290d", null ],
    [ "sparam_2_uint32_t_int32_t", "struct_g_l_c_d___command.html#a3d7feefe3a872bc4ed03bfc0944d5de0", null ],
    [ "sparam_3_uint32_t", "struct_g_l_c_d___command.html#ae2f05a0f24ab9cb6753308f1dc3c9117", null ],
    [ "sparam_4_uint32_t", "struct_g_l_c_d___command.html#a0464aa6f037598b80e825785e0c67919", null ],
    [ "sparam_4_uint32_t_const_uint8_t_p", "struct_g_l_c_d___command.html#a5c618a9dd81808f1e5f6b45ef63a32b6", null ],
    [ "sparam_5_uint32_t", "struct_g_l_c_d___command.html#aa2b078cd8a5aad169e5ecdb659c48222", null ],
    [ "sparam_bool", "struct_g_l_c_d___command.html#ab110f4a8757fbe18b2bb987d9216d1c2", null ],
    [ "sparam_GLCD_FONT", "struct_g_l_c_d___command.html#a4803e834ee71c7fc9b0a1db5dcbd820d", null ],
    [ "sparam_uint32_t", "struct_g_l_c_d___command.html#a9388a7d4d899e8353dded3a706c75d77", null ],
    [ "uparameters", "struct_g_l_c_d___command.html#ab594c8d1e3a2f12de0728ded33a0080e", null ]
];