var structx_s_t_r_e_a_m___b_u_f_f_e_r =
[
    [ "pucBuffer", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#aa8c28e51282198825d5ad0c2d666623a", null ],
    [ "ucFlags", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#ab8f6da769d17b9653e9a6d879e875e9b", null ],
    [ "xHead", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#a003e281d830468b1faabf4ba820f5ad0", null ],
    [ "xLength", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#a1ba6056c7e598ac40b4542c3b1614315", null ],
    [ "xTail", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#a6eedf625958216bba9136d98dcfed196", null ],
    [ "xTaskWaitingToReceive", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#aae9dce0a57e217492d5511b8f728d165", null ],
    [ "xTaskWaitingToSend", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#adcac505e0e57d53096673aae517ba651", null ],
    [ "xTriggerLevelBytes", "structx_s_t_r_e_a_m___b_u_f_f_e_r.html#a9cc97a6ea4374a84aa4d1453459ffce4", null ]
];