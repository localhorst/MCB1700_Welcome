var struct_audio_state =
[
    [ "cpcExpectedText", "struct_audio_state.html#a95e20b0683222f0a0154b664fe8b26d1", null ],
    [ "cpu32NextAction", "struct_audio_state.html#a58f99b7f7c82bb64b70fb077693a349a", null ],
    [ "cpu32Stack", "struct_audio_state.html#ab9de92af1ea026e2c9b42783c13c6bd6", null ],
    [ "highState", "struct_audio_state.html#a9de1dbcc48f9544b16e1851460a3b517", null ],
    [ "u16RemainingWaves", "struct_audio_state.html#a1158ab59103ac9eed7637fc2457170ba", null ],
    [ "u32EndBreak", "struct_audio_state.html#a91a01b3042b58a493ddf427e34660e41", null ],
    [ "u32StackPointer", "struct_audio_state.html#adeb3c4a732f9d13fad4516f5c5dcd3cb", null ]
];