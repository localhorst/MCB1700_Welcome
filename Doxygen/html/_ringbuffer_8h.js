var _ringbuffer_8h =
[
    [ "Ringbuffer", "struct_ringbuffer.html", "struct_ringbuffer" ],
    [ "RINGBUFFER_EMPTY", "_ringbuffer_8h.html#afdfa50d7e9394ac62325063cb1a0eb52", null ],
    [ "RINGBUFFER_ERROR", "_ringbuffer_8h.html#abe07e23cd7b8d0840243d1caba83a68e", null ],
    [ "RINGBUFFER_FULL", "_ringbuffer_8h.html#a01c6ee13c2fb1930324d4c1370e85ad5", null ],
    [ "RINGBUFFER_OKAY", "_ringbuffer_8h.html#aa18512c49f6077982315928e3e67e740", null ],
    [ "SIZEELEMENTSRINGBUFFER", "_ringbuffer_8h.html#a2ce2039e42dc33bf039bb75053c200d7", null ],
    [ "psRingbuffer_t", "_ringbuffer_8h.html#ae4a935353352a0542cf4805878c8defd", null ],
    [ "sRingbuffer_t", "_ringbuffer_8h.html#a0bbb3177e759c8bf45c088aef4ac318f", null ],
    [ "s32RingbufferGet", "_ringbuffer_8h.html#af4523e70b758e5f0757f4da559094c4c", null ],
    [ "s32RingbufferSet", "_ringbuffer_8h.html#a15fd4156953078c4d40daa7aa8064c4c", null ],
    [ "vRingbufferInit", "_ringbuffer_8h.html#a6cae50f2165a1bd166083c95abd347d5", null ]
];