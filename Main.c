/*! @file Main.c

  @brief   app main
  @author  Hendrik Schutter
  @version V1.0
  @date    14.11.2020

This file contains the main app function and init.
*/

#include "audio.h"
#include "Board_DAC.h"
#include "Welcome.h"
#include "Board_ADC.h"

//25x25 Dot Qr Code, 25bits per line (x)
uint32_t au32QrCode[25] =
{
    0x1FC897F,
    0x105BE41,
    0x174E55D,
    0x175A85D,
    0x174555D,
    0x1054B41,
    0x1FD557F,
    0x1400,
    0x1F7DBAA,
    0xF08422,
    0x15B31B,
    0x48E1C1,
    0xEFA8D7,
    0x15BDCAA,
    0x12703FB,
    0x1589271,
    0x1161DF4,
    0x1C918,
    0x1FDB557,
    0x1042118,
    0x175E3F7,
    0x1759A5F,
    0x1758D0D,
    0x1055BB9,
    0x1FDD43F
};

static void vMainInitApp(void);
static void vPrintQr(void);
static bool bCheckButton(void);
static void vUpdateVolume(void);
//static void SysTickHandler(void);

/**
 * @fn int main (void)
 * @brief app entry point
 * @param void
 * @return int
 * @author hendrik schutter
 * @date 14.11.2020
 */
int main (void)
{
    vMainInitApp(); //init app
    vPrintQr();	//print Qr

    vStartAudio();

    while(!bCheckButton()) {
        ADC_StartConversion();
        vUpdateLyrics();
        vUpdateVolume();
    }

    LED_Off(0);
    LED_Off(1);
    LED_Off(2);
    LED_Off(3);
    LED_Off(4);
    LED_Off(5);
    LED_Off(6);
    LED_Off(7);
    LPC_TIM0->TCR = 0; // stop music
}

/**
 * @fn void vMainInitApp(void)
 * @brief initialize app
 * @param void
 * @return void
 * @author hendrik schutter
 * @date 14.11.2020
 */
static void vMainInitApp(void)
{
    // Init audio pin
    DAC_Initialize();

    // Init GLCD
    GLCD_Initialize ();
    GLCD_SetForegroundColor (GLCD_COLOR_BLACK);
    GLCD_SetBackgroundColor (GLCD_COLOR_WHITE);
    GLCD_ClearScreen ();
    GLCD_SetFont(&GLCD_Font_16x24); // declared as extern in externals.h
    GLCD_DrawString (4*GLCD_Font_16x24.width, 5,"fckaf.de/2fs");

    // Init LED
    LED_Initialize();

    Buttons_Initialize();

    ADC_Initialize();

}

/**
 * @fn static void vPrintQr(void)
 * @brief print Qr code  from array
 * @param void
 * @return void
 * @author hendrik schutter
 * @date 14.11.2020
 */
static void vPrintQr(void) {

    uint8_t u8DotSize = 7U;
    uint8_t u8Yoffset = 30U;
    uint8_t u8Xoffset = 72U;

    for(uint8_t y = 0; y < 25; y++) {
        for(int8_t x = 0; x < 25; x++) {
            if(au32QrCode[y] & (1 << x))
            {
                //print dots u8DotSize X u8DotSize
                for(uint8_t u8DotLine = 0; u8DotLine < u8DotSize; u8DotLine++)
                {
                    //print horizontal line, swap left to right
                    GLCD_DrawHLine (-(x-24)*u8DotSize+u8Xoffset, y*u8DotSize+u8DotLine+u8Yoffset, u8DotSize);
                }
            }
        }
    }
}

static bool bCheckButton(void)
{
    static bool lastState;
    const bool btnState = Buttons_GetState();
    static uint16_t u16Debunce;


    if(u16Debunce < 1000U)
    {
        u16Debunce++;
    }

    if (btnState) //btn pressed
    {
        if(lastState && (u16Debunce >= 300U))
        {
            lastState = false;
            u16Debunce = 0;
            return false;
        }

        if((!lastState) && (u16Debunce == 1000U))
        {
            lastState = true;
            u16Debunce = 0;
            return true;
        }

    }
    return lastState;
}

static void vUpdateVolume(void)
{
    if(ADC_ConversionDone() == 0)
    {
        uint32_t u32ADCValue = ADC_GetValue();

        if(u32ADCValue <= 512)
        {
            LED_Off(0);
            LED_Off(1);
            LED_Off(2);
            LED_Off(3);
            LED_Off(4);
            LED_Off(5);
            LED_Off(6);
            LED_On(7);
            u32Volume = 16;
            return;
        }

        if(u32ADCValue <= 1024)
        {
            LED_Off(0);
            LED_Off(1);
            LED_Off(2);
            LED_Off(3);
            LED_Off(4);
            LED_Off(5);
            LED_On(6);
            LED_On(7);

            u32Volume = 64;
            return;
        }

        if(u32ADCValue <= 1536)
        {
            LED_Off(0);
            LED_Off(1);
            LED_Off(2);
            LED_Off(3);
            LED_Off(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 144;
            return;
        }

        if(u32ADCValue <= 2048)
        {
            LED_Off(0);
            LED_Off(1);
            LED_Off(2);
            LED_Off(3);
            LED_On(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 256;
            return;
        }

        if(u32ADCValue <= 2560)
        {
            LED_Off(0);
            LED_Off(1);
            LED_Off(2);
            LED_On(3);
            LED_On(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 400;
            return;
        }

        if(u32ADCValue <= 3072)
        {
            LED_Off(0);
            LED_Off(1);
            LED_On(2);
            LED_On(3);
            LED_On(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 576;
            return;
        }

        if(u32ADCValue <= 3584)
        {
            LED_Off(0);
            LED_On(1);
            LED_On(2);
            LED_On(3);
            LED_On(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 784;
            return;
        }

        if(u32ADCValue <= 4090)
        {
            LED_On(0);
            LED_On(1);
            LED_On(2);
            LED_On(3);
            LED_On(4);
            LED_On(5);
            LED_On(6);
            LED_On(7);
            u32Volume = 1023;
            return;
        }

    }


}




