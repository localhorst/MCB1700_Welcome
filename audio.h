/*! @file audio.h


  @brief   play sound
  @author  Lukas Fürderer
  @version V1.0
  @date    15.11.2020

This file contains the main app function and init.
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <stdbool.h>

void vStartAudio(void);
void vUpdateLyrics(void);

#endif
