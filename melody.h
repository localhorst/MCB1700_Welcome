/*! @file melody.h

  @brief   store  Never Gonna Give You Up
  @author  Lukas Fürderer
  @version V1.0
  @date    15.11.2020

This file contains the main app function and init.
*/

#ifndef MELODY_H
#define MELODY_H

#include "stdint.h"

extern const uint32_t *cpu32EntryPoint;
extern const uint32_t **cppu32JumpLabels;
extern const char *cpcTexts[];

#endif
